class Walker{
  float xPos;
  float yPos;
  
  void render(){
    circle(xPos, yPos, 30);
  }

  void toWalkRandom()
  {
    //reference: https://processing.org/discourse/beta/num_1193163956.html
    fill(random(255), random(255), random(255), random(255));
    stroke(random(255), random(255), random(255), random(255));
    
    int decision = floor(random(8));
    
    if (decision == 1){
    xPos--;
    yPos--;
    }
    else if (decision == 2){
    xPos++;
    yPos++;
    }
    else if (decision == 3){
    xPos--;
    yPos++;
    }
    else if (decision == 4){
    xPos--; 
    }
    else if (decision == 5){
    xPos++;
    yPos--;
    }
    else if (decision == 6){
    yPos--;
    }
    else if (decision == 7){
    xPos++;
    }
    else if (decision == 8){
    yPos++;
    }
  }
}
