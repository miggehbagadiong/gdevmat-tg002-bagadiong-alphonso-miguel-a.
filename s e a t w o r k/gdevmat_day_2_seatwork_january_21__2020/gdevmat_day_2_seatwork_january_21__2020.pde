/*
Bagadiong, Alphonso Miguel A.
TG002 | Seatwork Activity | January 21, 2020
*/

Walker walker = new Walker();

void setup(){
  size(1080,720,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),
        0,0,0,
        0,-1,0);
}

void draw(){
  walker.render();
  walker.toWalkRandom();
}
