/*
sine wave activity
bagadiong, alphonso miguel a.
tg002 gdevmat
*/

void setup(){
size(1080,720,P3D);
camera(0,0,-(height/2)/tan(PI*30/180),
      0,0,0,
      0,-1,0);
      
//render sine wave setup
wid = width + 16;
dx = (TWO_PI/period) * xspacing;
yvalues = new float[wid/xspacing];

}

void draw(){

  background(130);
  //circle(0,0,50);
  drawCartessianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  calculateSineWave();
  drawSineWave();
}

//cartessian plane
void drawCartessianPlane(){

  stroke(0,0,0);
  line(300,0,-300,0);
  line(0,300,0,-300);
 
 for(int i=-300; i<=300;i+=10){
   line(i,-5,i,5);
   line(-5,i,5,i);
 }
}

//linear function
void drawLinearFunction(){
  
  stroke(0,102,204);
  for(int x = -200; x <=200; x++){
    circle (x, x+2, 1);
  }
}

void drawQuadraticFunction(){
  
  stroke(255,0,0);
  for( float x=-300; x<=300; x+=0.1){
    circle(x*10,(float)Math.pow(x,2)+(2*x)-5,1);
}
}

//circle
float radius = 50;

void drawCircle(){
  
  stroke(1000,1000,0);
  for(int x=0; x<360;x++){
    circle((float)Math.cos(x)*radius,(float)Math.sin(x)*radius,1); 
  }
}

/*
sine wave
link: https://processing.org/examples/sinewave.html
*/

//sine wave variables
int xspacing = 16; //distance of each horizontal location
int wid; //entire wave width

float theta = 1.0; //distance of each horizontal location
float amplitude = 50.0; //entire wave width
float period = 500.0; //angle initial
float dx; //value to increment x
float[] yvalues; //use array to store height values

void calculateSineWave(){
//increment theta
theta+=0.1;

//every x value should be calculated with a y value using sine func
float x = theta;
for (int i = 0; i < yvalues.length; i++){
yvalues[i] = sin(x) * amplitude;
x+=dx;
}
}

void drawSineWave(){
stroke(0,0,0);
fill(255);

//simple way to draw wave with elipse at each location
for (int x = 0; x < yvalues.length; x++){
ellipse(300+(-x*xspacing/2), height/200+yvalues[x], 8, 8);
}
}
